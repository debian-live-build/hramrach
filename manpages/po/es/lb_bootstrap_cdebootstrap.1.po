# Spanish translations for live-build
# Copyright (C) 2013 Carlos Zuferri <chals@altorricon.com>
# This file is distributed under the same license as the live-build package.
#
msgid ""
msgstr ""
"Project-Id-Version: live-build 4.0~a19-1\n"
"POT-Creation-Date: 2013-05-27 16:47+0300\n"
"PO-Revision-Date: 2010-05-24 09:59+0300\n"
"Last-Translator: Carlos Zuferri <chals@altorricon.com>\n"
"Language-Team: none\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: TH
#: en/lb_bootstrap_cdebootstrap.1:9 en/lb_bootstrap_debootstrap.1:9
#, no-wrap
msgid "LIVE-BUILD"
msgstr "LIVE-BUILD"

#. type: TH
#: en/lb_bootstrap_cdebootstrap.1:9 en/lb_bootstrap_debootstrap.1:9
#, no-wrap
msgid "2013-05-27"
msgstr "27.05.2013"

#. type: TH
#: en/lb_bootstrap_cdebootstrap.1:9 en/lb_bootstrap_debootstrap.1:9
#, no-wrap
msgid "4.0~a19-1"
msgstr "4.0~a19-1"

#. type: TH
#: en/lb_bootstrap_cdebootstrap.1:9 en/lb_bootstrap_debootstrap.1:9
#, no-wrap
msgid "Live Systems Project"
msgstr "Proyecto Live Systems"

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:11 en/lb_bootstrap_debootstrap.1:11
#, no-wrap
msgid "NAME"
msgstr "NOMBRE"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:13 en/lb_bootstrap_debootstrap.1:13
msgid "B<live-build> - System Build Scripts"
msgstr "B<live-build> - Scripts de Creación del Sistema"

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:14 en/lb_bootstrap_debootstrap.1:14
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINOPSIS"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:16
msgid ""
"B<lb bootstrap_cdebootstrap> [--cdebootstrap-"
"options=I<CDEBOOTSTRAP_OPTIONS>] [--verbose]"
msgstr ""
"B<lb bootstrap_cdebootstrap> [--cdebootstrap-"
"options=I<OPCIONES_CDEBOOTSTRAP>] [--verbose]"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:18
msgid "B<lb bootstrap_cdebootstrap> [--help]"
msgstr "B<lb bootstrap_cdebootstrap> [--help]"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:20
msgid "B<lb bootstrap_cdebootstrap> [--version]"
msgstr "B<lb bootstrap_cdebootstrap> [--version]"

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:21 en/lb_bootstrap_debootstrap.1:21
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIPCIÓN"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:23 en/lb_bootstrap_debootstrap.1:23
msgid ""
"live-build contains the programs to build a live system from a configuration "
"directory."
msgstr ""
"live-build contiene los programas para construir un sistema en vivo a partir "
"de un directorio de configuración."

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:25
msgid ""
"The B<lb bootstrap_cdebootstrap> program bootstraps the chroot system with "
"cdebootstrap(1)."
msgstr ""
"El programa B<lb bootstrap_cdebootstrap> preinstala el chroot del sistema "
"con cdebootstrap(1)."

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:26 en/lb_bootstrap_debootstrap.1:26
#, no-wrap
msgid "OPTIONS"
msgstr "OPCIONES"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:27
#, no-wrap
msgid "--cdebootstrap-options=I<CDEBOOTSTRAP_OPTIONS>"
msgstr "--cdebootstrap-options=I<OPCIONES_CDEBOOTSTRAP>"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:29
msgid ""
"set cdebootstrap(1) options (default: None), see I<cdebootstrap>(1) for more "
"information about cdebootstrap."
msgstr ""
"establece las opciones de cdebootstrap(1) (por defecto: Ninguna), ver "
"I<cdebootstrap>(1) para más información acerca de cdebootstrap."

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:29 en/lb_bootstrap_debootstrap.1:29
#, no-wrap
msgid "--verbose"
msgstr "--verbose"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:31 en/lb_bootstrap_debootstrap.1:31
msgid "sets verbose option (default: False)"
msgstr "establece la opción verbose (por defecto: False)"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:31 en/lb_bootstrap_debootstrap.1:31
#, no-wrap
msgid "-h, --help"
msgstr "-h, --help"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:33 en/lb_bootstrap_debootstrap.1:33
msgid "show help message and exit"
msgstr "muestra el mensaje de ayuda y sale."

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:33 en/lb_bootstrap_debootstrap.1:33
#, no-wrap
msgid "-v, --version"
msgstr "-v, --version"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:35 en/lb_bootstrap_debootstrap.1:35
msgid "show program's version number and exit"
msgstr "muestra el número de versión del programa y sale."

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:36 en/lb_bootstrap_debootstrap.1:36
#, no-wrap
msgid "FILES"
msgstr "FICHEROS"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:37 en/lb_bootstrap_debootstrap.1:37
#, no-wrap
msgid "B<config/build>"
msgstr "B<config/build>"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:38 en/lb_bootstrap_debootstrap.1:38
#, no-wrap
msgid "B<cache/bootstrap>"
msgstr "B<cache/bootstrap>"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:39 en/lb_bootstrap_debootstrap.1:39
#, no-wrap
msgid "B<cache/packages.bootstrap>"
msgstr "B<cache/packages.bootstrap>"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:40 en/lb_bootstrap_debootstrap.1:40
#, no-wrap
msgid "B<chroot>"
msgstr "B<chroot>"

#. type: IP
#: en/lb_bootstrap_cdebootstrap.1:41 en/lb_bootstrap_debootstrap.1:41
#, no-wrap
msgid "B<.build/bootstrap>"
msgstr "B<.build/bootstrap>"

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:43 en/lb_bootstrap_debootstrap.1:43
#, no-wrap
msgid "SEE ALSO"
msgstr "VER TAMBIÉN"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:45
msgid "I<cdebootstrap>(1)"
msgstr "I<cdebootstrap>(1)"

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:46 en/lb_bootstrap_debootstrap.1:46
#, no-wrap
msgid "HOMEPAGE"
msgstr "PÁGINA WEB"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:48 en/lb_bootstrap_debootstrap.1:48
msgid ""
"More information about live-build and the Live Systems project can be found "
"on the homepage at E<lt>I<http://live-systems.org/>E<gt> and in the manual "
"at E<lt>I<http://live-systems.org/manual/>E<gt>."
msgstr ""
"Se puede encontrar más información acerca de live-build y el proyecto Live "
"Systems en la página web E<lt>I<http://live-systems.org/>E<gt> y en el "
"manual en E<lt>I<http://live-systems.org/manual/>E<gt>."

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:49 en/lb_bootstrap_debootstrap.1:49
#, no-wrap
msgid "BUGS"
msgstr "ERRORES"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:51 en/lb_bootstrap_debootstrap.1:51
msgid ""
"Bugs can be reported by submitting a bugreport for the live-build package in "
"the Bug Tracking System at E<lt>I<http://bugs.debian.org/>E<gt> or by "
"writing a mail to the Live Systems mailing list at E<lt>I<debian-live@lists."
"debian.org>E<gt>."
msgstr ""
"Se puede notificar los fallos enviando un informe de errores sobre el "
"paquete live-build en el Bug Tracking System en E<lt>I<http://bugs.debian."
"org/>E<gt> o escribiendo un mensaje a la lista de correo de Live Systems a "
"la dirección E<lt>I<debian-live@lists.debian.org>E<gt>."

#. type: SH
#: en/lb_bootstrap_cdebootstrap.1:52 en/lb_bootstrap_debootstrap.1:52
#, no-wrap
msgid "AUTHOR"
msgstr "AUTOR"

#. type: Plain text
#: en/lb_bootstrap_cdebootstrap.1:53 en/lb_bootstrap_debootstrap.1:53
msgid ""
"live-build was written by Daniel Baumann E<lt>I<mail@daniel-baumann.ch>E<gt>."
msgstr ""
"live-build fue escrito por Daniel Baumann E<lt>I<mail@daniel-baumann."
"ch>E<gt>."
